<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';

bab_functionality::includefile('Icons');


/**
 * Provides the Awesome icon theme.
 */
class Func_Icons_Awesome extends Func_Icons
{
    /**
     * @return string
     * @static
     */
    public function getDescription()
    {
        return 'Provides the Awesome icon theme.';
    }


    /**
     * Register myself as a functionality.
     * @static
     */
    public function register()
    {
        require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
        $functionalities = new bab_functionalities();
        $functionalities->registerClass(__CLASS__, __FILE__);
    }


    /**
     * Includes all necessary CSS files to the current page.
     *
     * @return bool		false in case of error
     */
    public function includeCss()
    {
        $babBody = bab_getBody();

        $useCDN = bab_Registry::get('/core/useCDN', true);
        if ($useCDN) {
            $fontFile = 'font.css';
        } else {
            $fontFile = 'font-local.css';
        }
        $stylePath = bab_getAddonInfosInstance('icons_awesome')->getStylePath();
        if (substr($stylePath, 0, strlen('vendor')) == 'vendor') {
            $babBody->addStyleSheet($stylePath . 'icons/' . $fontFile);
            $babBody->addStyleSheet($stylePath . 'icons/icons.css');
        } else {
            $babBody->addStyleSheet('addons/icons_awesome/icons/' . $fontFile);
            $babBody->addStyleSheet('addons/icons_awesome/icons/icons.css');
        }
        return true;
    }
}
